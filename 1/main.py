
def first():
    most_calories = 0
    current_elf_sum = 0
    with open('input.txt') as f:
        for line in f.readlines():
            line = line.replace('\n', '')
            if line:
                current_elf_sum += int(line)
            else:
                if current_elf_sum > most_calories:
                    most_calories = current_elf_sum
                current_elf_sum = 0

    return most_calories


def second():
    sums = []
    current_elf_sum = 0
    with open('input.txt') as f:
        for line in f.readlines():
            line = line.replace('\n', '')
            if line:
                current_elf_sum += int(line)
            else:
                sums.append(current_elf_sum)
                current_elf_sum = 0
    return sum(sorted(sums)[-3:])


print(first())
print(second())
